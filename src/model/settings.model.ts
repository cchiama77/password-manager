
import mongoose, { Schema, Model, Document } from 'mongoose';

type SettingsDocument = Document & {
  name: string;
  description: string | null;
};

type SettingsInput = {
  name: SettingsDocument['name'];
  description: SettingsDocument['description'];
};

const settingsSchema = new Schema(
  {
    name: {
      type: Schema.Types.String,
      required: true,
      unique: true,
    },
    description: {
      type: Schema.Types.String,
      default: null,
    },
  },
  {
    collection: 'Settings',
    timestamps: true,
  },
);

const Settings: Model<SettingsDocument> = mongoose.model<SettingsDocument>('Settings', settingsSchema);

export { Settings, SettingsDocument, SettingsInput };