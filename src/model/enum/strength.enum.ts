enum PasswordStrength {
  EASY,
  MEDIUM,
  WEAK,
  STRONG,
}

type PasswordStrengthStrings = keyof typeof PasswordStrength;

/**
 * 
 * @param key 
 * @param message 
 */
const printImportant = (key: PasswordStrengthStrings, message: string) => {
  const level = PasswordStrength[key];
  if (level <= PasswordStrength.EASY) {
    console.log("Password Strength key is:", key);
    console.log("Password Strength value is:", level);
    console.log("Password Strength message is:", message);
  }
};
