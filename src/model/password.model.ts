import mongoose, { Schema, Model, Document } from "mongoose";

type PasswordDocument = Document & {
  value: string;
  strength: PasswordStrength;
  description: string | null;
};

type PasswordInput = {
  name: PasswordDocument["value"];
  strength: PasswordDocument["strength"];
  description: PasswordDocument["description"];
};

const passwordSchema = new Schema(
  {
    value: {
      type: Schema.Types.String,
      required: true,
      unique: true,
    },
    strength: {
      type: PasswordStrength,
      default: null,
    },
    description: {
      type: Schema.Types.String,
      default: null,
    },
  },
  {
    collection: "roles",
    timestamps: true,
  }
);

const Password: Model<PasswordDocument> = mongoose.model<PasswordDocument>(
  "Password",
  passwordSchema
);

export { Password, PasswordDocument, PasswordInput };
