import { UserController } from "@/controller/user.controller";
import { createExpressServer } from "routing-controllers";
import { BaseController } from "./controller/base.controller";
const PORT = 4000;

console.info(`Starting server on http://localhost:${PORT}`);

const routes = [BaseController, UserController];

const app = createExpressServer({
  controllers: routes,
  classTransformer: true,
  cors: {
    origin: "*", // (note: do not use this in production)
  },
});

app.listen(PORT);
