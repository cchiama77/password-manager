import { Inject } from "typedi";

@Inject()
export abstract class BaseService<S> {
  name: S;
}
