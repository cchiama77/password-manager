export class ServiceResponse<K> {
    data: K;
    statusCode: number;
    error: any;

    constructor(data: K, statusCode: number = 200, error: any = null) {
        this.data = data;
        this.statusCode = statusCode;
        this.error = error;
    }


    static of<K>(data: K) {
        return new ServiceResponse(data, 200, null);
    }

    static createErrorResponse(statusCode: number = 400, error: any) {
        return new ServiceResponse(null, statusCode, error);
    }

    isSuccess() {
        return this.statusCode >= 200 && this.statusCode < 300;
    }
    isFailure() {
        return this.statusCode == (400 | 401 | 403 | 500 | 501 | 503);
    }
}